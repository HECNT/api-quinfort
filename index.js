var express = require('express')
var app     = express()
var bodyParser = require('body-parser');
const { Pool, Client } = require('pg')
const connectionString = 'postgres://kygxcjviavjzlq:c1840f925ba494a6f189ff3babe91bf464ed9f98ff738ac76d5a736c8fa9e4de@ec2-23-23-159-84.compute-1.amazonaws.com:5432/dd1ulcesav8ak6?ssl=true'
const client = new Client({
  connectionString: connectionString,
})
client.connect()

app.use(bodyParser.json());
app.use(bodyParser());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
})

app.set('port', (process.env.PORT || 6001));

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
  res.send('API Activa')
})

app.get('/get', doGet)
app.post('/set', doSet)
app.post('/del', doDel)
app.post('/ses', doSes)

function doGet(req, res) {
  var consulta = `
  SELECT
    *
  FROM
    programador
  `;
  client.query(consulta, (err, result) => {
    if (err) {
      client.end
      res.json({err:true, description:err})
    } else {
      client.end
      res.json({err:false, res:result.rows})
    }
  })
}

function doSet(req, res) {
  var d = req.body;
  var consulta = `
  INSERT INTO
    programador (nombre, apellido, lenguaje_id, edad)
  VALUES
    ($1, $2, $3, $4)
  `;
  var values = [d.nombre, d.apellido, d.lenguaje_id, d.edad]
  client.query(consulta, values,(err, result) => {
    if (err) {
      client.end
      res.json({err:true, description:err})
    } else {
      client.end
      res.json({err:false, res:result})
    }
  })
}

function doDel(req, res) {
  var d = req.body;
  var consulta = `
    DELETE FROM
      programador
    WHERE
      id = $1
  `;
  var values = [d.id];
  client.query(consulta, values, function (err, result){
    if (err) {
      client.end
      res.json({err:true, description:err})
    } else {
      client.end
      res.json({err:false, res:result})
    }
  })
}

function doSes(req, res) {
  var d = req.body;
  var usr = 'admin'
  var pass = '1234'
  if (d.username == usr && d.password == pass) {
    res.json({err:false})
  } else {
    res.json({err:true, description: 'Credenciales invalidadas'})
  }
}

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
